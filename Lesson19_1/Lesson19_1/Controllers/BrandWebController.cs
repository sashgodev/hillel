﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lesson19_1.Models.Brand;
using Lesson19_1.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Lesson19_1.Controllers
{
    public class BrandWebController : Controller
    {
        private IBrandService _brandService;
        public BrandWebController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        public IActionResult AddBrand()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddBrand(CreateBrandRequest brand)
        {
            await _brandService.AddBrandAsymc(brand);

            return Redirect("~/Home");
        }
    }
}
