﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lesson19_1.Models;
using Lesson19_1.Models.Brand;
using Lesson19_1.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Lesson19_1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BrandController : ControllerBase
    {
        //BrandController
        private IBrandService _brandService;
        private ILogger<BrandController> _logger;

        public BrandController(IBrandService brandService, ILogger<BrandController> logger)
        {
            _brandService = brandService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<BrandResponse>> Get()
        {
            _logger.LogInformation("Requested Get");
            return await _brandService.GetBrandsAsync();
        }

        //baseurl/brand/id
        [HttpGet("{id}")]
        public async Task<ServerResponse<Brand>> Get(int id)
        {
            var brand =  await _brandService.GetBrandAsync(id);
            if (brand == null)
            {
                return new ServerResponse<Brand> { IsSuccess = false, Error = $"Brand not found with id={id}"};
            }
            return new ServerResponse<Brand> { IsSuccess = true, Result = brand };
        }


        [HttpPost]
        public async Task  Post(CreateBrandRequest brand)
        {
            await _brandService.AddBrandAsymc(brand);
        }

        [HttpDelete]
        public async Task Delete (int id)
        {
            await _brandService.DeleteBrandAsync(id);
        }
    }
}
