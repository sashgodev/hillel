﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lesson19_1.Models;
using Lesson19_1.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Lesson19_1.Controllers
{
    public class ModelWebController : Controller
    {
        private IModelService _modelService;

        public ModelWebController(IModelService modelService)
        {
            _modelService = modelService;
        }
        public async Task<IActionResult> ModelEdit(int id)
        {
            var models = await _modelService.GetModelsAsync();
            var m = models.FirstOrDefault(m => m.Id == id);

            return View(m);
        }

        [HttpPost] 
        public async Task<IActionResult> ModelEdit(Model model) 
        {
            await _modelService.UpdateModelAsync(model);
            return Redirect("~/Home/Models");
        }
    }
}
