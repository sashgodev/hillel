﻿using Lesson19_1.Models;
using Lesson19_1.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lesson19_1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ModelController: ControllerBase
    {
        private IModelService _modelService;

        public ModelController(IModelService modelService, ILogService logService)
        {
            _modelService = modelService;
        }

        [HttpGet]
        public Task<IList<ModelResponse>> Get()
        {
            return  _modelService.GetModelsAsync();
        }

        [HttpPost]
        public async Task Post(ModelRequest model)
        {
            await _modelService.AddModelAsync(model);
        }
    }
}
