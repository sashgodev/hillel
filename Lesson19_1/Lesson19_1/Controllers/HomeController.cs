﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lesson19_1.Models.Brand;
using Lesson19_1.Services;
using Microsoft.AspNetCore.Mvc;

namespace Lesson19_1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBrandService _brandService;
        private readonly IModelService _modelService;

        public HomeController(IBrandService brandService, IModelService modelService)
        {
            _brandService = brandService;
            _modelService = modelService;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["Description"] = "List of brands";
            ViewBag.Title = "Home";

            var brands = await _brandService.GetBrandsAsync();

            return View(brands);
        }

        public async Task<IActionResult> Models()
        {
            var models = await _modelService.GetModelsAsync();
            return View(models);
        }
    }
}