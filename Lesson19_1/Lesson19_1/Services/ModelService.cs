﻿using AutoMapper;
using Lesson19_1.DataModels;
using Lesson19_1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lesson19_1.Services
{
    public class ModelService : IModelService
    {
        private readonly MyDbContext _dbContext;
        private readonly IMapper _mapper;

        public ModelService(ILogService logService,
            MyDbContext dbContext,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        public async Task AddModelAsync(ModelRequest model)
        {
            var data = new ModelData { Name = model.Name};


            await _dbContext.ModelData.AddAsync(data);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IList<ModelResponse>> GetModelsAsync()
        {
            IList<ModelResponse> result = null;

            var models = await _dbContext.ModelData.Include(b => b.BrandData).ToListAsync();
            result = _mapper.Map<IList<ModelResponse>>(models);

            return result;
        }

        public async Task UpdateModelAsync(Model model)
        {
            var data = _mapper.Map<ModelData>(model);
            _dbContext.ModelData.Update(data);
            await _dbContext.SaveChangesAsync();
        }
    }
}
