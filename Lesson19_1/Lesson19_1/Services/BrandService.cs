﻿using AutoMapper;
using Lesson19_1.DataModels;
using Lesson19_1.Helpers;
using Lesson19_1.Models;
using Lesson19_1.Models.Brand;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lesson19_1.Services
{
    public class BrandService : IBrandService
    {
        private readonly MyDbContext _dbContext;
        private readonly IMapper _mapper;

        public BrandService(ILogService logService,
            MyDbContext dbContext,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task AddBrandAsymc(CreateBrandRequest brand)
        {
            var data = new BrandData { Name = brand.Name, Description = brand.Description};


            await _dbContext.BrandData.AddAsync(data);
            await _dbContext.SaveChangesAsync();

            //_brands.Add(brand);
        }

        public async Task DeleteBrandAsync(int id)
        {
            var data = _dbContext.BrandData.FirstOrDefault(b => b.Id == id);
            if (data != null)
            {
                _dbContext.BrandData.Remove(data);
                await _dbContext.SaveChangesAsync();
            }
        }

        public Task<Brand> GetBrandAsync(int id)
        {   
            return Task.FromResult(new Brand());
        }

        public async Task<IList<BrandResponse>> GetBrandsAsync()
        {
            IList<BrandResponse> result = null;

            var brands = await _dbContext.BrandData.ToListAsync();
            result = _mapper.Map<IList<BrandResponse>>(brands);

            return result;
        }
    }
}
