﻿using Lesson19_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lesson19_1.Services
{
    public interface IModelService
    {
        Task<IList<ModelResponse>> GetModelsAsync();
        Task AddModelAsync(ModelRequest model);

        Task UpdateModelAsync(Model model);

    }
}
