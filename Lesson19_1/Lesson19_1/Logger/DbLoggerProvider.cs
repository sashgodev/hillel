﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lesson19_1.Logger
{
    public class DbLoggerProvider : ILoggerProvider
    {
        private readonly IApplicationBuilder _app;
        public DbLoggerProvider(IApplicationBuilder app)
        {
            _app = app;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new DbLogger(_app, categoryName);
        }

        public void Dispose()
        {
            
        }
    }
}
