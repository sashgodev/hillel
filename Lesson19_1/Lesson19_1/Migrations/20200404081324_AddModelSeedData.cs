﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Lesson19_1.Migrations
{
    public partial class AddModelSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BrandDataId",
                table: "ModelData",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "ModelData",
                columns: new[] { "Id", "BrandDataId", "Name" },
                values: new object[] { 1, 999, "FX555" });

            migrationBuilder.CreateIndex(
                name: "IX_ModelData_BrandDataId",
                table: "ModelData",
                column: "BrandDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelData_BrandData_BrandDataId",
                table: "ModelData",
                column: "BrandDataId",
                principalTable: "BrandData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelData_BrandData_BrandDataId",
                table: "ModelData");

            migrationBuilder.DropIndex(
                name: "IX_ModelData_BrandDataId",
                table: "ModelData");

            migrationBuilder.DeleteData(
                table: "ModelData",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "BrandDataId",
                table: "ModelData");
        }
    }
}
