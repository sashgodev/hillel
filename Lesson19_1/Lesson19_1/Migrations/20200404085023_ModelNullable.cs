﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Lesson19_1.Migrations
{
    public partial class ModelNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelData_BrandData_BrandDataId",
                table: "ModelData");

            migrationBuilder.AlterColumn<int>(
                name: "BrandDataId",
                table: "ModelData",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelData_BrandData_BrandDataId",
                table: "ModelData",
                column: "BrandDataId",
                principalTable: "BrandData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelData_BrandData_BrandDataId",
                table: "ModelData");

            migrationBuilder.AlterColumn<int>(
                name: "BrandDataId",
                table: "ModelData",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelData_BrandData_BrandDataId",
                table: "ModelData",
                column: "BrandDataId",
                principalTable: "BrandData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
