﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lesson19_1.DataModels
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
            Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<BrandData>().HasData(new BrandData { Id = 999, Name = "LG", Description = "TV brand" });
            modelBuilder.Entity<ModelData>().HasData(new ModelData { Id = 1, Name = "FX555", BrandDataId =  999});
            modelBuilder.Entity<LogData>();
        }
        public DbSet<BrandData> BrandData { get; set; }
        
        public DbSet<ModelData> ModelData { get; set; }

        public DbSet<LogData> LogData { get; set; }
    }
}
